
public class Vector2D {
	double x;
	double y;
	
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public Vector2D(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void rotate(double theta) {
		theta = Math.toRadians(theta);
		double sin = Math.sin(theta);
		double cos = Math.cos(theta);
		double _x = x * cos - y * sin;
		double _y = x * sin + y * cos;
		
		x = _x;
		y = _y;
	}
	
	public void toInt() {
		x = Math.round(x);
		y = Math.round(y);
	}
	
	@Override
	public String toString() {
		return "["+x+","+y+"]";
	}
	
	
	public static void main(String[] args) {
		Vector2D v = new Vector2D(1,0);
		for(int i = 0; i<8;i++) {
			System.out.println(v);
			v.rotate(45);
			v.toInt();
		}
	}
}
